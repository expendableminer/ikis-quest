﻿using UnityEngine;
using System.Collections;
using System;

public delegate void MouseMoved(float xMovement, float yMovement);
public delegate void TouchMoved(float xTouch, float yTouch);
public class InputManager : MonoBehaviour
{
    private float _xMovement;
    private float _yMovement;
    private float _xTouch;
    private float _yTouch;

    public static event MouseMoved MouseMoved;
    public static event TouchMoved TouchMoved;

    private float startX;
    private float targetX;

    private static void OnMouseMoved(float xmovement, float ymovement)
    {
        var handler = MouseMoved;
        if (handler != null)
            handler(xmovement, ymovement);
    }

    private static void OnTouchMoved(float xtouch, float ytouch)
    {
        var handler = TouchMoved;
        if (handler != null)
            handler(xtouch, ytouch);
    }

    private void InvokeActionOnInput()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            _xMovement = Input.GetAxis("Mouse X");
            OnMouseMoved(_xMovement, _yMovement);
        }


#elif ANDROID || UNITY_ANDROID || UNITY_IPHONE
        try
        {

            //if (Input.GetTouch(0).phase == TouchPhase.Moved)
           // {
             //   _xTouch = Input.GetAxis("Touch X");
            //    OnTouchMoved(_xTouch, _yTouch);
          //  }

            if (Input.GetTouch(0).phase == TouchPhase.Moved) 

//#elif ANDROID || UNITY_ANDROID || UNITY_IPHONE

        for (int i = 0; i < Input.touchCount; ++i)
            switch (Input.GetTouch(i).phase)

            {
                case TouchPhase.Began:
                    // save touch start location
                    startX = Input.GetAxis("Touch X");
                    OnTouchBegan(); // enables bool to start lerping in update?
                    break;
                case TouchPhase.Moved:
                    // save touch target location
                    targetX = Input.GetAxis("Touch X") - startX;
                    OnTouchMoved(targetX, 0.0f);
                    break;
                case TouchPhase.Ended:
                    OnTouchEnded();
                    break;

            }


        }
        catch (ArgumentException e)
        {
            Debug.Log(e.Message);
        }
#endif

        //try
        //{
        //    if (Input.GetTouch(0).phase == TouchPhase.Moved) 
        //    {
        //        _xTouch = Input.GetAxis("Touch X");
        //        OnTouchMoved(_xTouch, _yTouch);
        //    }
        //}
        //catch (ArgumentException e)
        //{
        //    Debug.Log(e.Message);
        //}
//#endif
    }

    void Update()
    {
        InvokeActionOnInput();
    }
}
