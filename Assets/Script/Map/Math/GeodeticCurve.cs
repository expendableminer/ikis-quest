/* Gavaghan.Geodesy by Mike Gavaghan
 * 
 * http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/
 * 
 * This code may be freely used and modified on any personal or professional
 * project.  It comes with no warranty.
 *
 * BitCoin tips graciously accepted at 1FB63FYQMy7hpC2ANVhZ5mSgAZEtY1aVLf
 */
using System;
using System.Runtime.Serialization;


/// <summary>
/// This is the outcome of a geodetic calculation.  It represents the path and
/// ellipsoidal distance between two GlobalCoordinates for a specified reference
/// ellipsoid.
/// </summary>
    [Serializable]
    public struct GeodeticCurve : ISerializable
    {
        /// <summary>
        /// Create a new GeodeticCurve.
        /// </summary>
        /// <param name="ellipsoidalDistanceMeters">ellipsoidal distance in meters</param>
        /// <param name="azimuth">azimuth in degrees</param>
        /// <param name="reverseAzimuth">reverse azimuth in degrees</param>
        public GeodeticCurve(double ellipsoidalDistanceMeters, Angle azimuth, Angle reverseAzimuth)
        {
            this.EllipsoidalDistanceMeters = ellipsoidalDistanceMeters;
            this.Azimuth = azimuth;
            this.ReverseAzimuth = reverseAzimuth;
        }

        /// <summary>Ellipsoidal distance (in meters).</summary>
        public double EllipsoidalDistanceMeters;

        /// <summary>
        /// Get the azimuth.  This is angle from north from start to end.
        /// </summary>
        public Angle Azimuth;

        /// <summary>
        /// Get the reverse azimuth.  This is angle from north from end to start.
        /// </summary>
        public Angle ReverseAzimuth;

        

        #region Serialization / Deserialization

        private GeodeticCurve(SerializationInfo info, StreamingContext context)
        {
            this.EllipsoidalDistanceMeters = info.GetDouble("ellipsoidalDistanceMeters");

            double azimuthRadians = info.GetDouble("azimuthRadians");
            double reverseAzimuthRadians = info.GetDouble("reverseAzimuthRadians");

            this.Azimuth = Angle.FromRadians(azimuthRadians);
            this.ReverseAzimuth = Angle.FromRadians(reverseAzimuthRadians);
        }

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("ellipsoidalDistanceMeters", this.EllipsoidalDistanceMeters);

            info.AddValue("azimuthRadians", this.Azimuth.Radians);
            info.AddValue("reverseAzimuthRadians", this.ReverseAzimuth.Radians);
        }

        #endregion

        #region Operators

        public static bool operator ==(GeodeticCurve lhs, GeodeticCurve rhs) { return Equals(lhs, rhs); }
        public static bool operator !=(GeodeticCurve lhs, GeodeticCurve rhs) { return !Equals(lhs, rhs); }

        #endregion
    }

