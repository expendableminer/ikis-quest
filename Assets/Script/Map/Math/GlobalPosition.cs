/* Gavaghan.Geodesy by Mike Gavaghan
 * 
 * http://www.gavaghan.org/blog/free-source-code/geodesy-library-vincentys-formula/
 * 
 * This code may be freely used and modified on any personal or professional
 * project.  It comes with no warranty.
 *
 * BitCoin tips graciously accepted at 1FB63FYQMy7hpC2ANVhZ5mSgAZEtY1aVLf
 */
using System;
using System.Runtime.Serialization;


    [Serializable]
    public struct GlobalPosition : ISerializable
    {
        /// <summary>
        /// Creates a new instance of GlobalPosition for a position on the surface of
        /// the reference ellipsoid.
        /// </summary>
        /// <param name="coords"></param>
        public GlobalPosition(GlobalCoordinates coords)
            : this(coords, 0)
        {
        }

        /// <summary>
        /// Creates a new instance of GlobalPosition.
        /// </summary>
        /// <param name="coords">coordinates on the reference ellipsoid.</param>
        /// <param name="elevationMeters">elevation, in meters, above the reference ellipsoid.</param>
        public GlobalPosition(GlobalCoordinates coords, double elevationMeters)
        {
            this.Coordinates = coords;
            this.ElevationMeters = elevationMeters;
            Longitude = this.Coordinates.Longitude;
            Latitude = this.Coordinates.Latitude;
        }

        /// <summary>Get global coordinates.</summary>
        public GlobalCoordinates Coordinates;

        /// <summary>Get latitude.</summary>
        public Angle Latitude;

        /// <summary>Get longitude.</summary>
        public Angle Longitude;

        /// <summary>
        /// Get elevation, in meters, above the surface of the reference ellipsoid.
        /// </summary>
        public double ElevationMeters;

        public static int Compare(GlobalPosition first, GlobalPosition second)
        {
            int a = GlobalCoordinates.Compare(first.Coordinates, second.Coordinates);

            return a == 0
                ? first.ElevationMeters.CompareTo(second.ElevationMeters)
                : a;
        }

       

        #region Serialization / Deserialization

        //private GlobalPosition(SerializationInfo info, StreamingContext context)
        //{
        //    this.ElevationMeters = info.GetDouble("elevationMeters");

        //    double longitudeRadians = info.GetDouble("longitudeRadians");
        //    double latitudeRadians = info.GetDouble("latitudeRadians");

        //    Angle longitude = Angle.FromRadians(longitudeRadians);
        //    Angle latitude = Angle.FromRadians(latitudeRadians);

        //    this.Coordinates = new GlobalCoordinates(longitude, latitude);
        //}

        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("elevationMeters", this.ElevationMeters);

            info.AddValue("longitudeRadians", this.Coordinates.Longitude.Radians);
            info.AddValue("latitudeRadians", this.Coordinates.Latitude.Radians);
        }

        #endregion

        #region Operators

        public static bool operator ==(GlobalPosition lhs, GlobalPosition rhs) {return Equals(lhs, rhs); }
        public static bool operator !=(GlobalPosition lhs, GlobalPosition rhs) {return !Equals(lhs, rhs); }
        public static bool operator <(GlobalPosition lhs, GlobalPosition rhs) {return Compare(lhs, rhs) < 0; }
        public static bool operator <=(GlobalPosition lhs, GlobalPosition rhs) {return Compare(lhs, rhs) <= 0; }
        public static bool operator >(GlobalPosition lhs, GlobalPosition rhs) {return Compare(lhs, rhs) > 0; }
        public static bool operator >=(GlobalPosition lhs, GlobalPosition rhs) { return Compare(lhs, rhs) >= 0; }

        #endregion
    }

