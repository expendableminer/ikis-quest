﻿using UnityEngine;
using System.Runtime;
using System.Collections;


public struct Laton
{
    public float Latitude;
    public float Longitude; 

    public Laton(float latitude, float longtitude)
    {
        Latitude = latitude;
        Longitude = longtitude;
    }
}

public enum Corner
{
    TopRight,
    TopLeft,
    BottomRight,
    BottomLeft,
}

public static class GPSMath
{
    private static readonly int EARTH_RADIUS_KM = 6371000;

    public static float ToRad(float input)
    {
        return input * (Mathf.PI / 180);
    }

    public static Vector3 MakePositive(Vector3 input)
    {
        if (input.x < 0)
        {
            input.x *= -1;
        }
        
        if(input.z < 0)
        {
            input.z *= -1;
        }

        return input;
    }

    public static void UnityLocation(Laton CurrGPS, GameObject CompareObject1 ,GameObject CompareObject2, out Laton unityLocation)
    {
        GPSCompareBeacon CompareBeacon1 = CompareObject1.GetComponent<GPSCompareBeacon>();
        GPSCompareBeacon CompareBeacon2 = CompareObject2.GetComponent<GPSCompareBeacon>();

        Corner CompareObjectCorner1 = CompareBeacon1.corner;
        Corner CompareObjectCorner2 = CompareBeacon2.corner;

        float LocX = 0, LocZ = 0;

        if (CompareBeacon1 && CompareBeacon2)
        {
            if (CornerCorrect(CurrGPS, CompareBeacon1.laton, CompareObjectCorner1) && CornerCorrect(CurrGPS, CompareBeacon2.laton, CompareObjectCorner2))
            {
                Vector3 Beacon1Location = CompareObject1.transform.position;
                Vector3 Beacon2Location = CompareObject2.transform.position;

                float DiffX, DiffZ;

                //float DiffrenceLat = CompareBeacon1.Latidude - CompareBeacon2.Latidude;
                //float DiffrenceLong = CompareBeacon1.Longitude - CompareBeacon2.Longitude;

                //if (DiffrenceLat < 0)
                //{
                //    DiffrenceLat *= -1;
                //}

                //if (DiffrenceLong < 0)
                //{
                //    DiffrenceLong *= -1;
                //}

                float PercentageLat, PercentageLong;

                GetPercentage(CurrGPS, CompareBeacon1, CompareBeacon2, CompareObjectCorner1, out PercentageLong, out PercentageLat);

                if (PercentageLat != 0 && PercentageLong != 0)
                {
                    DiffrenceBeaconLocations(Beacon1Location, CompareObjectCorner1, Beacon2Location, CompareObjectCorner2, out DiffX, out DiffZ);

                    if(DiffX < 0)
                    {
                        DiffX *= -1;
                    }
                    
                    if(DiffZ < 0)
                    {
                        DiffZ *= -1;
                    }

                    float ActualDiffX, ActualDiffZ;

                    ActualDiffX = (DiffX * (PercentageLat / 100));
                    ActualDiffZ = (DiffZ * (PercentageLong / 100));

                    NewLocation(ActualDiffX, ActualDiffZ, Beacon1Location, CompareObjectCorner1, Beacon2Location, CompareObjectCorner2, out LocX, out LocZ);
                }
            }
        }
        //unityLocation = new Laton();

        unityLocation.Latitude = LocX;
        unityLocation.Longitude = LocZ;
    }

    public static void GetPercentage(Laton CurrGPS, GPSCompareBeacon CompareBeacon1, GPSCompareBeacon CompareBeacon2, Corner corner, out float PerLong, out float PerLat)
    {
        switch (corner)
        {
            case Corner.TopRight:
                {
                    PerLat = ((CurrGPS.Latitude - CompareBeacon1.laton.Latitude) / (CompareBeacon2.laton.Latitude - CompareBeacon1.laton.Latitude)) * 100;
                    PerLong = ((CurrGPS.Longitude - CompareBeacon2.laton.Longitude) / (CompareBeacon1.laton.Longitude - CompareBeacon2.laton.Longitude)) * 100;
                }
                break;
            case Corner.TopLeft:
                {
                    PerLat = ((CurrGPS.Latitude - CompareBeacon2.laton.Latitude) / (CompareBeacon1.laton.Latitude - CompareBeacon2.laton.Latitude)) * 100;
                    PerLong = ((CurrGPS.Longitude - CompareBeacon2.laton.Longitude) / (CompareBeacon1.laton.Longitude - CompareBeacon2.laton.Longitude)) * 100;
                }
                break;
            case Corner.BottomRight:
                {
                    PerLat = ((CurrGPS.Latitude - CompareBeacon1.laton.Latitude) / (CompareBeacon2.laton.Latitude - CompareBeacon1.laton.Latitude)) * 100;
                    PerLong = ((CurrGPS.Longitude - CompareBeacon1.laton.Longitude) / (CompareBeacon2.laton.Longitude - CompareBeacon1.laton.Longitude)) * 100;
                }
                break;
            case Corner.BottomLeft:
                {
                    PerLat = ((CurrGPS.Latitude - CompareBeacon2.laton.Latitude) / (CompareBeacon1.laton.Latitude - CompareBeacon2.laton.Latitude)) * 100;
                    PerLong = ((CurrGPS.Longitude - CompareBeacon1.laton.Longitude) / (CompareBeacon2.laton.Longitude - CompareBeacon1.laton.Longitude)) * 100;
                }
                break;
            default:
                {
                    PerLat = 0;
                    PerLong = 0;
                }
                break;
        }
    }

    private static bool CornerCorrect(Laton Curr, Laton Compare, Corner corner)
    {
        bool Corr = false;

        switch(corner)
        {
            case Corner.TopRight:
                {
                    if(Curr.Latitude < Compare.Latitude && Curr.Longitude < Compare.Longitude)
                    {
                        Corr = true;
                    }
                }
                break;
            case Corner.TopLeft:
                {
                    if (Curr.Latitude > Compare.Latitude && Curr.Longitude < Compare.Longitude)
                    {
                        Corr = true;
                    }
                }
                break;
            case Corner.BottomRight:
                {
                    if (Curr.Latitude < Compare.Latitude && Curr.Longitude > Compare.Longitude)
                    {
                        Corr = true;
                    }
                }
                break;
            case Corner.BottomLeft:
                {
                    if (Curr.Latitude > Compare.Latitude && Curr.Longitude > Compare.Longitude)
                    {
                        Corr = true;
                    }
                }
                break;
            default:
                break;

        }

        return Corr;
    }

    public static void DiffrenceBeaconLocations(Vector3 Beacon1, Corner Corner1, Vector3 Beacon2, Corner Corner2, out float DiffX, out float DiffZ)
    {
        switch(Corner1)
        {
            case Corner.TopRight:
                {
                    DiffZ = Beacon1.z - Beacon2.z;
                    DiffX = Beacon2.x - Beacon1.x;
                }
                break;
            case Corner.TopLeft:
                {
                    DiffZ = Beacon1.z - Beacon2.z;
                    DiffX = Beacon1.x - Beacon2.x;
                }
                break;
            case Corner.BottomRight:
                {
                    DiffZ = Beacon2.z - Beacon1.z;
                    DiffX = Beacon2.x - Beacon1.x;
                }
                break;
            case Corner.BottomLeft:
                {
                    DiffZ = Beacon2.z - Beacon1.z;
                    DiffX = Beacon1.x - Beacon2.x;
                }
                break;
            default:
                {
                    DiffX = DiffZ = 0;
                }
                break;

        }
    }

    public static void NewLocation(float DiffX, float DiffZ, Vector3 Beacon1, Corner Corner1, Vector3 Beacon2, Corner Corner2, out float LocationX, out float LocationZ)
    {
        switch(Corner1)
        {
            case Corner.TopRight:
                {
                    LocationX = Beacon1.x - DiffX;
                    LocationZ = Beacon1.z - DiffZ;
                }
                break;
            case Corner.TopLeft:
                {
                    LocationX = Beacon2.x - DiffX;
                    LocationZ = Beacon2.z + DiffZ;
                }
                break;
            case Corner.BottomRight:
                {
                    LocationX = Beacon1.x - DiffX;
                    LocationZ = Beacon2.z - DiffZ;
                }
                break;
            case Corner.BottomLeft:
                {
                    LocationX = Beacon2.x - DiffX;
                    LocationZ = Beacon2.z - DiffZ;
                }
                break;
            default:
                {
                    LocationX = LocationZ = 0;
                }
                break;
        }
    }

    public static float Haversine(Laton New, Laton Old, float LatMod = 1)
    {
        if (New.Latitude - Old.Latitude != 0)
        {
            if (New.Latitude > Old.Latitude)
            {
                New.Latitude -= (New.Latitude - Old.Latitude) * LatMod;
            }
            else
            {
                New.Latitude += (New.Latitude - Old.Latitude) * LatMod;
            }
        }

        float DLat = ToRad(New.Latitude - Old.Latitude);
        float DLong = ToRad(New.Longitude - Old.Longitude);

        //if(New.Latitude > Old.Latitude)
        //{
        //    New.Latitude -= DLat * 100;
        //}
        //else
        //{
        //    New.Latitude += DLat * 100;
        //}

        Debug.Log(DLong);
        Debug.Log(DLat);

        float CentralAngle = Mathf.Pow(Mathf.Sin((float)DLat / 2), 2) + Mathf.Cos(ToRad(Old.Latitude)) * Mathf.Cos(ToRad(New.Latitude)) * Mathf.Pow(Mathf.Sin((float)DLong / 2), 2);

        float circle = /*Mathf.Atan2(Vincety, 1 - Vincety);*/ 2 * Mathf.Atan2(Mathf.Sqrt((float)CentralAngle), Mathf.Sqrt((float)(1 -CentralAngle)));

        float distance = ((float)EARTH_RADIUS_KM * circle);

        return distance;

        //return Mathf.Acos(Mathf.Sin(Old.Latitude) * Mathf.Sin(New.Latitude) + Mathf.Cos(Old.Latitude) * Mathf.Cos(New.Latitude) * Mathf.Cos(DLong)) * EARTH_RADIUS_KM; 

    #region CalculationsNotNeeded
        //float Vincety = (Mathf.Pow(Mathf.Cos(ToRad(New.Latitude) * Mathf.Sin(DLong)),2) + Mathf.Pow((Mathf.Cos(ToRad(Old.Latitude)) 
        //    * Mathf.Sin(ToRad(New.Latitude))) - (Mathf.Sin(ToRad(Old.Latitude)) * Mathf.Cos(ToRad(New.Latitude)) *
        //    Mathf.Cos(DLong)) , 2)) / ((Mathf.Sin(ToRad(Old.Latitude)) * Mathf.Sin(ToRad(New.Latitude))) + 
        //    (Mathf.Cos(ToRad(Old.Latitude) * Mathf.Cos(ToRad(New.Latitude)) * Mathf.Cos(DLong))));

        //GeodeticCalculator calc = new GeodeticCalculator();

        //GeodeticMeasurement result = calc.CalculateGeodeticMeasurement(Ellipsoid.WGS84, new GlobalPosition(new GlobalCoordinates(Angle.FromDegrees(New.Latitude), Angle.FromDegrees(New.Longitude))), new GlobalPosition(new GlobalCoordinates(Angle.FromDegrees(New.Latitude), Angle.FromDegrees(New.Longitude))));

        //int distance = result.PointToPointDistanceMeters;
    #endregion
}

    public static float CoordinateAngle(Laton NewCoord, Laton OldCoord, float distance)
    {
        float NewLat = OldCoord.Latitude + (distance / (EARTH_RADIUS_KM * 1000)) * (180 * Mathf.PI);

        float Xo = OldCoord.Longitude;
        float Yo = OldCoord.Latitude;

        float Xn = NewCoord.Longitude;
        float Yn = NewCoord.Latitude;

        float Xd = OldCoord.Longitude;
        float Yd = NewLat;

        float Xno = Xn - Xo;
        float Yno = Yn - Yo;
        float Xdo = Xd - Xo;
        float Ydo = Yd - Yo;

        float Atan2n = Mathf.Atan2((float)Xno, (float)Yno);
        float Atan2d = Mathf.Atan2((float)Xdo, (float)Ydo);

        return (Atan2n - Atan2d) * (-180 / Mathf.PI);

#region DoesntWorkForMe
        //double Dlong = (NewCoord.Longitude - OldCoord.Longitude);

        //double y = Mathf.Sin((float)Dlong) * Mathf.Cos(NewCoord.Latitude);
        //double x = Mathf.Cos(OldCoord.Latitude) * Mathf.Sin(NewCoord.Latitude) - Mathf.Sin(OldCoord.Latitude) * Mathf.Cos(NewCoord.Latitude) * Mathf.Cos((float)Dlong);

        //double brng = Mathf.Atan2((float)y, (float)x);

        //brng = ToRad((float)brng);
        //brng = (brng + 360) % 360;
        //brng = 360 - brng;

        //return brng; 
#endregion
    }

    public static Vector3 GetPointFromAngleAndDistance(Vector3 origin, double angle, double distance)
    {
        //Vector3 NewPos = new Vector3((float)distance * Mathf.Sin((float)angle), 0, (float)distance * Mathf.Cos((float)angle));

        //Vector3 NewWorldPos = NewPos + origin;

        //Quaternion NewPos = Quaternion.AngleAxis((float)angle, Vector3.forward);

        //Vector3 NewWorldPos = origin + NewPos * Vector3.right * (float)distance;

        float x = (float)distance * Mathf.Cos((float)angle * Mathf.Deg2Rad);
        float z = (float)distance * Mathf.Sin((float)angle * Mathf.Deg2Rad);

        Vector3 NewWorldPos = origin;

        NewWorldPos.x += x;
        NewWorldPos.z += z; 

        return NewWorldPos;
    }
}
