﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class GPSMovement : MonoBehaviour
{
    private CharacterController Controller;

    private float speed; 
 

    public void initialize(CharacterController Controller)
    {
        this.Controller = Controller;
    }

    // Use this for initialization
    void Start()
    {

    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public Laton UpdateLocation(Laton CurrLocation, GameObject CompareBeacon1, GameObject CompareBeacon2)
    {
#if Caprisun
//float distance = CalculateDiffrenceGPSLocation(NewCoord, OldCoord, LatMod) * modifier;

        //float Angle = 180 - GPSMath.CoordinateAngle(NewCoord, OldCoord, distance);

        //Vector3 NewWorldPos = GPSMath.GetPointFromAngleAndDistance(transform.position, Angle, distance);

#if Debug
        print(string.Format("angle is: {0}", Angle));

        print(string.Format("new Vector is {0}, {1}, {2}", NewWorldPos.x, NewWorldPos.y, NewWorldPos.z));
#endif

        //MovementDebug debug = new MovementDebug();

        //debug.distance = distance;

        //Debug.Log(distance);
        //debug.newLocation = NewWorldPos;
        //debug.angle = Angle;

        //return debug;
#endif

        Laton CurrLoc = new Laton();

        GPSMath.UnityLocation(CurrLocation, CompareBeacon1, CompareBeacon2, out CurrLoc);

        return CurrLoc;
    }

    public float CalculateDiffrenceGPSLocation(Laton NewCoord, Laton OldCoord, float LatMod = 1)
    {    
        return GPSMath.Haversine(NewCoord, OldCoord, LatMod);
    }

    
}
