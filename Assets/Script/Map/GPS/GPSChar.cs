﻿using UnityEngine;
using System.Collections;

public enum MovementReturns
{
    Moved,
    FailedValidation,
    WrongCoord,
    FailedToMove
}

public struct MovementDebug
{
    public MovementReturns DataReturned;

    public Vector3 newLocation;
    public double distance;
    public double angle; 
}

public class GPSChar : GPSBase
{
    protected GPSMovement Movement;

    private CharacterController Controller;
    private Vector3 moveDirection = Vector3.zero;

    //private NavMeshAgent agent;
    
    private Animator animator; 

    //public GameObject EmptyPRefab; 

    public float Speed;

    protected bool PathComplete;

    MovementDebug MovementReturn;

    private Laton StartingCoord = new Laton(53.212275f, 6.565754f);

    private float WaitForIt = 5;

    public float CurrLa, CurrLo;

    public bool Test = true;

    public Laton NewLocation;

    public GameObject CompareBeacon1, CompareBeacon2;

    // Use this for initialization
    void Start()
    {
        StartCoroutine(StartGPS(false));

        //print(string.Format("{0}",GPSMath.Haversine(new Laton(53.212140f, 6.565778f), new Laton(53.212265f, 6.565773f))));

        Controller = GetComponent<CharacterController>();

        Movement = gameObject.AddComponent<GPSMovement>() as GPSMovement;//new GPSMovement(Controller); 
        Movement.initialize(Controller);

        //agent = gameObject.GetComponent<NavMeshAgent>();

        Updated = false;
	}

    // Update is called once per frame
    void Update()
    {
        WaitForIt -= Time.deltaTime;

        if(Test && CompareBeacon1 && CompareBeacon2)
        //if (Input.location.status == LocationServiceStatus.Running && Started == true && WaitForIt < 1)
        {
            Test = false;
            StartCoroutine(UpdateGPS());
            UpdateLocation();
        } 
        
        
        if (transform.position != MovementReturn.newLocation && MovementReturn.newLocation != Vector3.zero)
        {
            if (logtest.Contains("Moving") != true)
                logtest += "|Moving|";

            Move();
        }
        else
        {
            logtest.Replace("Moving", "Moved");
        }  
    }

    void OnApplicationQuit()
    {
        StartCoroutine(StopGPS(true));
    }
    
    void OnApplicationPause(bool paused)
    {
        if (paused)
            StartCoroutine(StopGPS(false));
        else
            StartCoroutine(StartGPS(true));
    }



    void UpdateLocation()
    {
        //if (!Updated)
        //  return; 

        //Updated = false;

        //MovementReturn = Movement.UpdateLocation(new Laton(PreviousLocationLat, previousLocationLong), new Laton(CurrentLocationLat, CurrentLocationLong));

        NewLocation = Movement.UpdateLocation(new Laton(CurrLa, CurrLo),CompareBeacon1, CompareBeacon2);

        MovementReturn.newLocation = new Vector3(NewLocation.Latitude, transform.position.y, NewLocation.Longitude);
    }

    void Move()
    {
        float step = Speed * Time.deltaTime;
        transform.position = Vector3.MoveTowards(transform.position, new Vector3(NewLocation.Latitude, transform.position.y, NewLocation.Longitude), step);
    }

}
