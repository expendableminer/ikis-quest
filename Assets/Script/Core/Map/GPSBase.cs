﻿#define Debug

using UnityEngine;
using System.Collections;
using System.IO;

public class GPSBase : MonoBehaviour
{

    protected float beginLocationLong;
    protected float beginLocationLat;

    public float CurrentLocationLong;
    public float CurrentLocationLat;

    protected float previousLocationLong;
    protected float PreviousLocationLat;

    protected bool Updated;

    protected string logtest = "Testing|";

    protected bool Started = false;

    public float LatMod = 1;

    //protected bool FirstLocUpdate;

    void Awake()
    {

    }

    // Use this for initialization
    void Start()
    {
        DontDestroyOnLoad(this);
        

#if Debug 
        print("test");
#endif
    }

    // Update is called once per frame
    void Update()
    {

    }

    public IEnumerator StartGPS(bool Resume)
    {
        if (Started == false)
        {
            if (!Input.location.isEnabledByUser && Input.location.status == LocationServiceStatus.Stopped)
            {
                logtest += "failed";
                yield break;
            }

            Input.location.Start(1, 2);

            logtest += Input.location.status.ToString() + "|";

            int wait = 20;
            while (Input.location.status == LocationServiceStatus.Initializing && wait > 0)
            {
                //logtest = "";
                wait--;

                print("Connecting...");
                logtest += "|Connecting...|";
                yield return new WaitForSeconds(1);
            }

            if (wait < 1)
            {

                print("Service did not start");
                logtest += "|Service did not Start|";
                yield break;
            }

            if (Input.location.status == LocationServiceStatus.Failed)
            {

                print("Can't determine your location");
                logtest += "|Can't determine your location|";

                yield break;
            }
            else
            {
                if (!Resume)
                {
                    logtest += "|Connected|";
                    print("I have determined your location");

                    beginLocationLat = Input.location.lastData.latitude;
                    beginLocationLong = Input.location.lastData.longitude;
                }


                CurrentLocationLat = Input.location.lastData.latitude;
                CurrentLocationLong = Input.location.lastData.longitude;

                PreviousLocationLat = beginLocationLat;
                previousLocationLong = beginLocationLong;
                Started = true;

                logtest += string.Format("Long: {0}, Lat: {1}", CurrentLocationLong, CurrentLocationLat);
            }
        }

        yield break;
    }

    void OnGUI()
    {
        //GUI.Label(new Rect(Screen.width / 2, Screen.height / 2, 3000, 200), logtest);

        //ransform.position = Vector3.MoveTowards(transform.position, transform.position * 1.2f, 1 * Time.deltaTime);
        //logtest = "";
    }

    public IEnumerator UpdateGPS()
    {
        if (Input.location.isEnabledByUser && Input.location.status == LocationServiceStatus.Running && (Input.location.lastData.longitude != CurrentLocationLong || Input.location.lastData.latitude != CurrentLocationLat))
        {
            logtest = "";

            PreviousLocationLat = CurrentLocationLat;
            previousLocationLong = CurrentLocationLong;

            CurrentLocationLat = Input.location.lastData.latitude;
            CurrentLocationLong = Input.location.lastData.longitude;

            Updated = true;

            logtest += string.Format("Long: {0}, Lat: {1}, LongStart: {2}, LatStarting: {3}", CurrentLocationLong, CurrentLocationLat, beginLocationLong, beginLocationLat);

            PlayerPrefs.Save();

        }
        yield break;
    }

    public IEnumerator StopGPS(bool WipeData)
    {
        Input.location.Stop();

        if (WipeData)
        {
            beginLocationLat = 0;
            beginLocationLong = 0;

            CurrentLocationLat = 0;
            CurrentLocationLong = 0;
        }


        yield break;
    }
}
