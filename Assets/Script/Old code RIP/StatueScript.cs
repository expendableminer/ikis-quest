﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class StatueScript : MonoBehaviour
{
    public MoveCamera mc;

    void Update()
    {
        StatueTouched();
    }

    public void StatueTouched()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonUp(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 1000))
            {
                try
                {
                    if (hit.transform.tag == "Statue")
                    {
                        //Debug.Log("Statue hit");
                        mc = hit.transform.gameObject.GetComponent<MoveCamera>();
                        mc.StatueClicked = true;
                    }
                }
                catch (NullReferenceException ex)
                {
                    Debug.Log(ex.Message);
                }
            }
        }
//#elif ANDROID || UNITY_ANDROID
//        if (Input.GetTouch(0).phase == TouchPhase.Began)
//        {
//            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
//            RaycastHit hit;

//            if (Physics.Raycast(ray, out hit, 1000))
//            {
//                try
//                {
//                    if (hit.transform.tag == "Statue")
//                    {
//                        //Debug.Log("Statue hit");
//                        mc = hit.transform.gameObject.GetComponent<MoveCamera>();
//                        mc.StatueClicked = true;
//                    }
//                }
//                catch (NullReferenceException ex)
//                {
//                    //Debug.Log("No hit");
//                }
//            }
//        }
#elif ANDROID || UNITY_ANDROID || UNITY_IPHONE
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 1000))
            {
                try
                {
                    if (hit.transform.tag == "Statue")
                    {
                        //Debug.Log("Statue hit");
                        mc = hit.transform.gameObject.GetComponent<MoveCamera>();
                        mc.StatueClicked = true;
                    }
                }
                catch (NullReferenceException ex)
                {
                    Debug.Log(ex.Message);
                }
            }
        }
#endif
    }
}
