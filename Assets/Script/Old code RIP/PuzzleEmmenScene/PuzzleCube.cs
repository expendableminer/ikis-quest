﻿using UnityEngine;
using System.Collections;

public class PuzzleCube : MonoBehaviour
{
    private bool cubeClicked = true;

    public void StartRotating()
    {
        if (cubeClicked)
        {
            StartCoroutine(RotateMe(Vector3.up * 90f, 1f));
            cubeClicked = false;
        }
    }

    IEnumerator RotateMe(Vector3 byAngles, float inTime)
    {
        Quaternion fromAngle = transform.rotation;
        Quaternion toAngle = Quaternion.Euler(transform.eulerAngles + byAngles);
        for (float t = 0f; t < 1f; t += Time.deltaTime / inTime)
        {
            transform.rotation = Quaternion.Lerp(fromAngle, toAngle, t);
            yield return null;
        }
        cubeClicked = true;
    }
}
