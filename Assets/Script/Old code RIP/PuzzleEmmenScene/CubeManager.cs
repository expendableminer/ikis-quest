﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class CubeManager : MonoBehaviour
{
    public PuzzleCube pc;

    void Update()
    {
        CubeTouched();
    }

    public void CallRotation()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 1000))
        {
            try
            {
                if (hit.transform.tag == "Cube")
                {
                    pc = hit.transform.gameObject.GetComponent<PuzzleCube>();
                    pc.StartRotating();
                    //Debug.Log("Cube hit!");
                }
            }
            catch (NullReferenceException ex)
            {
                //Debug.Log("No hit");
            }
        }
    }

    public void CubeTouched()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonUp(0))
        {
            CallRotation();
        }

#elif ANDROID || UNITY_ANDROID
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            CallRotation();
        }
#endif
    }

    public void ReturnButton()
    {
        SceneManager.LoadScene("Alpha Map");
    }
}
