﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MoveCamera : MonoBehaviour
{
    public GameObject cam;
    public bool StatueClicked = false;
    public bool LockTouchInput = false;

    private float speed = 15;

    void Start()
    {
        LockTouchInput = true;
    }

    void Update()
    {
        if (StatueClicked)
        {
            LockTouchInput = false;
            MoveTheCamera();
        }
    }

    public void MoveTheCamera()
    {
        var targetRotation = Quaternion.LookRotation(this.transform.position - cam.transform.position);

        cam.transform.rotation = Quaternion.Slerp(cam.transform.rotation, targetRotation, speed * Time.deltaTime);
        float step = speed * Time.deltaTime;
        cam.transform.position = Vector3.MoveTowards(cam.transform.position, this.transform.position, step);

        //VERANDER DEZE NAAR DE GOEIE
        if (cam.transform.position == transform.position)
        {
            SceneManager.LoadScene("Emmen Puzzle");
        }
    }
}
